/**
 * Created by GBT on 16-01-2018.
 */
/* ===================================================================
 * Dazzle - Main JS
 *
 * ------------------------------------------------------------------- */


function myFunction() {
    document.getElementById("myDropdown1").classList.toggle("show");
}
function myFunction2() {
    document.getElementById("myDropdown2").classList.toggle("show");
}

(function($) {

    "use strict";


    $('.panel-heading a').click(function () {
        $('.panel-heading').removeClass('active');

        //If the panel was open and would be closed by this click, do not active it
        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in'))
            $(this).parents('.panel-heading').addClass('active');
    });


// Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if(!event || !event.target) {
            return
        }
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }




    var cfg = {
            scrollDuration : 800, // smoothscroll duration
            mailChimpURL   : 'http://facebook.us8.list-manage.com/subscribe/post?u=cdb7b577e41181934ed6a6a44&amp;id=e65110b38d' // mailchimp url
        },

        $WIN = $(window);

    // Add the User Agent to the <html>
    // will be used for IE10 detection (Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0))
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);


    /* Smooth Scrolling
     * ------------------------------------------------------ */
    var ssSmoothScroll = function() {
        $('.smoothscroll').on('click', function (e) {
            var target = this.hash,
                $target    = $(target);

            e.preventDefault();
            e.stopPropagation();
            if($target.offset() && $target.offset().top){
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top - 60
                }, cfg.scrollDuration, 'swing', function () {
                    window.location.hash = target;
                });
            }
        });

    };

    $('.navbar-collapse a').click(function (e) {
        $('.navbar-collapse').collapse('toggle');
    });



    /* Alert Boxes
     ------------------------------------------------------- */
    var ssAlertBoxes = function() {

        $('.alert-box').on('click', '.close', function() {
            $(this).parent().fadeOut(500);
        });

    };


    /* AjaxChimp
     * ------------------------------------------------------ */

    var validateEmail = function(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
    }

    var ssAjaxChimp = function() {

        // $('#mc-form').ajaxChimp({
        // 	language: 'es',
        //    	url: cfg.mailChimpURL
        // });

        $('#mc-form-submit').click(function(e) {
            e.preventDefault();
            $("#email-failure").hide();
            $("#email-success").hide();

            var email = $("#mc-email").val();
            if(email && validateEmail(email)) {
                var formdata = {}
                formdata["email"] = email;

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "/api/v1/user",
                    data: JSON.stringify(formdata),
                    success: function () {
                        $("#email-success").show();
                    },
                });
            } else {
                $("#email-failure").show();
            }
        })


        // Mailchimp translation
        //
        //  Defaults:
        //	 'submit': 'Submitting...',
        //  0: 'We have sent you a confirmation email',
        //  1: 'Please enter a value',
        //  2: 'An email address must contain a single @',
        //  3: 'The domain portion of the email address is invalid (the portion after the @: )',
        //  4: 'The username portion of the email address is invalid (the portion before the @: )',
        //  5: 'This email address looks fake or invalid. Please enter a real email address'

        // $.ajaxChimp.translations.es = {
        //   'submit': 'Submitting...',
        //   0: '<i class="fa fa-check"></i> We have sent you a confirmation email',
        //   1: '<i class="fa fa-warning"></i> You must enter a valid e-mail address.',
        //   2: '<i class="fa fa-warning"></i> E-mail address is not valid.',
        //   3: '<i class="fa fa-warning"></i> E-mail address is not valid.',
        //   4: '<i class="fa fa-warning"></i> E-mail address is not valid.',
        //   5: '<i class="fa fa-warning"></i> E-mail address is not valid.'
        // }

    };



    /* Back to Top
     * ------------------------------------------------------ */
    var ssBackToTop = function() {

        var pxShow  = 500,         // height on which the button will show
            fadeInTime  = 400,         // how slow/fast you want the button to show
            fadeOutTime = 400,         // how slow/fast you want the button to hide
            scrollSpeed = 300,         // how slow/fast you want the button to scroll to top. can be a value, 'slow', 'normal' or 'fast'
            goTopButton = $("#go-top")

        // Show or hide the sticky footer button
        $(window).on('scroll', function() {
            if ($(window).scrollTop() >= pxShow) {
                goTopButton.fadeIn(fadeInTime);
            } else {
                goTopButton.fadeOut(fadeOutTime);
            }
        });
    };


    /*
     Currency List
     */
    var handle_currency_api_call = function(data) {
        $(".conversion_currency_amount").removeClass("loading-input");
        if(data && data.hasOwnProperty("body") && data["body"].hasOwnProperty("amount") && data["body"]["amount"] > 0){
            $(".conversion_currency_amount").val(parseFloat(data["body"]["amount"]).toFixed(8));
        } else if (data && data.hasOwnProperty("body") && data["body"]["amount"] == 0 && data["body"]["minimum"] >= 0) {
            showAlert("Minimum amount required for conversion is " + data["body"]["minimum"] + " " + $(".selected_currency").attr("data-symbol"));
            $(".conversion_currency_amount").val(0);
        } else {
            showAlert("Our partner could not provide the exchange rate for "+ $(".selected_currency").attr("data-symbol").toUpperCase() + " to " + $(".conversion_currency").attr("data-symbol").toUpperCase() + ", please try again in few minutes.");
            $(".conversion_currency_amount").val(0);
        }
    }

    var select_recommended_currency_exchange = function(response) {
        var output;
        var result = response.options;
        var offers = 0;
        if (response.count >= 1) {
            offers = response.count - 1;
        }
        if(result && result.length >0) {
            var currentMax = -1;
//            var offers = -1;

            for(var i=0;i<result.length; i++){
                if(result[i] && result[i].is_supported &&  result[i].body && parseFloat(result[i].body.amount) >= 0 && parseFloat(result[i].body.amount) > currentMax) {
                    currentMax = parseFloat(result[i].body.amount);
//                    offers += 1;
                    output = result[i];
                }
            }
            $(result).each(function(index) {
                if(result[index] && result[index]["body"] && result[index]["body"]["recommended"]) {
                    output = result[index];
                }
            });
        } else {
            showAlert("There was an error fetching the rates, please refresh the page to try again.");
        }
        showOffersCount(offers)
        handle_currency_api_call(output);
    }

    var showOffersCount = function(offers) {
        if (offers > 0) {
            if (offers == 1) {
                $(".number-of-offers").html(offers + " More Offer");
                $(".number-of-offers").removeClass("offer-padding");
            } else {
                $(".number-of-offers").html(offers + " More Offers");
                $(".number-of-offers").removeClass("offer-padding");
            }
        } else {
            $(".number-of-offers").html("");
            $(".number-of-offers").addClass("offer-padding");
        }
    }

    var get_recommended_currency_exchange = function (to, from, amount) {
        var output;
        var result = get_currency_exchange(to, from, amount);
        if(result && result.length >0) {
            output = result[0];
            $(result).each(function(index) {
                if(result[index]["recommended"] && result[index]["recommended"]) {
                    output = result[index];
                }
            });
        } else {
            showAlert("There was an error fetching the rates, please refresh the page to try again.");
        }
        return output;
    }

    var get_currency_exchange =  function (to, from, amount) {
        var formData = {};
        formData["body"] = {}
        formData["body"]["from_currency"] = from;
        formData["body"]["to_currency"] = to;
        formData["body"]["amount"] = amount;
        var currency_list = window.currency_list;
        var from_coin = {};
        var to_coin = {};
        $(currency_list).each(function(index) {
            if (currency_list[index].symbol == from) {
                from_coin = currency_list[index]
            }
            if (currency_list[index].symbol == to) {
                to_coin = currency_list[index];
            }
        });

        $(".you-send-message").html(from_coin["display_name"]);
        $(".you-get-message").html(to_coin["display_name"]);

        var result;
        $(".conversion_currency_amount").val("");
        $(".conversion_currency_amount").addClass("loading-input")
        showOffersCount(0);
        if(isCurrentToSymbolValid(from, to)) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "/api/v1/get-exchange-amount",
                data: JSON.stringify(formData),
                success: function (data) {
                    select_recommended_currency_exchange(data);
                },
            });
        } else {
            showAlert(from_coin["display_name"] + " to " + to_coin["display_name"] + " is not supported currently. Please choose other combination.")
        }

    }

    var showAlert = function(message){
        var html = '<div class="note font-OS-Reg"><p style="margin: 0px;">' + message + '</p></div>'
        $("#home-page-warning").html(html);
        $("#home-page-warning").show();
    }

    var hideAlert = function(){
        $("#home-page-warning").css("display", "none");
    }

    var swap = function() {
        hideAlert();
        var to_currency_symbol = $(".conversion_currency").attr("data-symbol");
        var from_currency_symbol = $(".selected_currency").attr("data-symbol");
        $(".conversion_currency").attr("data-symbol", from_currency_symbol);
        $(".selected_currency").attr("data-symbol", to_currency_symbol);
        $("[name='to_currency']").val(from_currency_symbol);
        $("[name='from_currency']").val(to_currency_symbol);

        var to_currency_name = $(".conversion_currency").html();
        var from_currency_name = $(".selected_currency").html();
        $(".conversion_currency").html(from_currency_name);
        $(".selected_currency").html(to_currency_name);

        // Swap Image
        var to_img = $(".default_to_img").attr("src");
        $(".default_to_img").attr("src", $(".default_from_img").attr("src"));
        $(".default_from_img").attr("src", to_img);

        // Remove More Offers
        $(".number-of-offers").html("");
        $(".number-of-offers").addClass("offer-padding");

        // Swap you get/have
        var youGet = $(".you-get-message").html();
        $(".you-get-message").html($(".you-send-message").html());
        $(".you-send-message").html(youGet);

        $(".conversion_currency_amount").val("");
        $(".conversion_currency_amount").addClass("loading-input");
        handleToCurrencyList($("#toFilterCurrency").val());
        get_currency_exchange($(".conversion_currency").attr("data-symbol"), $(".selected_currency").attr("data-symbol"), $(".selected_currency_amount").val());

    }

    var handleFromCurrencyList = function(fromCurrencyFilter) {
        var currency_list = window.currency_list;
        var fromCurrencyFilter = fromCurrencyFilter || "";
        var fromCurrencyList = $.extend([], currency_list);
        if(fromCurrencyFilter){
            fromCurrencyList = $.grep(fromCurrencyList, function (e) {
                return e.symbol.toLowerCase().indexOf(fromCurrencyFilter.toLowerCase()) >= 0 || e.display_name.toLowerCase().indexOf(fromCurrencyFilter.toLowerCase()) >= 0;
            });
            if ((!fromCurrencyList || (fromCurrencyList && fromCurrencyList.length <= 0)) && window.mixpanel && window.is_prod) {
                mixpanel.track("From_Currency", {
                    "Currency": fromCurrencyFilter
                });
            }
        }

        var html_top_from = "";
        $(fromCurrencyList).each(function(index) {
            //'<a class="dropdown-item dropdown-item-from" data-id=\"' + currency_list[index]["symbol"] +  '\" data-name=\"' + currency_list[index]["selected_name"] + '\" href="#">' + currency_list[index]["display_name"] + '</a>' +

            html_top_from += '<li class="dropdown-item-from lg-pl-0" data-id=\"' + fromCurrencyList[index]["symbol"] +  '\" data-name=\"' + fromCurrencyList[index]["selected_name"] + '\" data-image=\"' + fromCurrencyList[index]["image_sm_url"] + '\">' +
                '<div class="row item__wrapper" style="height:42px;line-height: 42px;">' +
                '<div class="col-xs-4 coin_short text-right text-uppercase">' + fromCurrencyList[index]["symbol"] + '</div>' +
                '<div class="col-xs-2 icon__wrapper">' +
                '<img class="glyphicon coin_icon" height="20" width="20" src="' + fromCurrencyList[index]["image_sm_url"] +'" alt="xmr">' +
                '</div>' +
                '<div class="col-xs-6 coin_full" uib-tooltip="' +  fromCurrencyList[index]["display_name"] + 'tooltip-append-to-body="true" tooltip-placement="right" tooltip-popup-delay="10">' + fromCurrencyList[index]["display_name"] + '</div>' +
                '</div>' +
                '</li>';
        });
        $(".dropdown-menu-from").html(html_top_from);

        $(".dropdown-item-from").click(function(e) {
            e.preventDefault();
            window.is_inactive = false;
            hideAlert();
            var symbol = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            var image = $(this).attr("data-image");
            var to_symbol = $(".conversion_currency").attr("data-symbol");
            if(symbol == to_symbol) {
                swap();
            } else {
                $(".selected_currency").html(name);
                $(".selected_currency").attr("data-symbol", symbol);
                $(".default_from_img").attr("src", image);
                $("[name='from_currency']").val(symbol);
                handleToCurrencyList($("#toFilterCurrency").val());
                get_currency_exchange($(".conversion_currency").attr("data-symbol"), $(".selected_currency").attr("data-symbol"), $(".selected_currency_amount").val());
            }
        })
    };

    var isCurrentToSymbolValid = function (from_symbol, to_symbol) {
        if(coin_mapping) {
            return coin_mapping[from_symbol] && coin_mapping[from_symbol].length > 0 && coin_mapping[from_symbol].indexOf(to_symbol) >= 0;
        } else {
            return false;
        }

    };

    var handleToCurrencyList = function(toCurrencyFilter) {
        var currency_list = window.currency_list;
        var coin_mapping = window.coin_mapping;
        var from_currency_symbol = $(".selected_currency").attr("data-symbol");
        var toCurrencyFilter = toCurrencyFilter || "";
        var toCurrencyList = $.extend([], currency_list);
        if(toCurrencyList){
            toCurrencyList = $.grep(toCurrencyList, function (e) {
                return e.symbol.toLowerCase().indexOf(toCurrencyFilter.toLowerCase()) >= 0 || e.display_name.toLowerCase().indexOf(toCurrencyFilter.toLowerCase()) >= 0;
            });
        }
        var html_top_to = "";
        var html_not_supported = "";
        var shouldAddNotSupportedSection = false;
        var from_symbol = $(".selected_currency").attr("data-symbol");
        html_not_supported += '<li class="lg-pl-0 non-clickable"><div class="row item__wrapper"><div class="dropdown-header not-supported-message">Not supported from ' + from_currency_symbol.toUpperCase() + '</div></div></li>';

        var should_log_to_currency_search = 1;

        $(toCurrencyList).each(function(index) {
            //'<a class="dropdown-item dropdown-item-from" data-id=\"' + currency_list[index]["symbol"] +  '\" data-name=\"' + currency_list[index]["selected_name"] + '\" href="#">' + currency_list[index]["display_name"] + '</a>' +
            if (coin_mapping && coin_mapping[from_symbol] && coin_mapping[from_symbol].length > 0 && coin_mapping[from_symbol].indexOf(toCurrencyList[index]["symbol"]) >= 0){
                html_top_to += '<li class="dropdown-item-to lg-pl-0" data-id=\"' + toCurrencyList[index]["symbol"] +  '\" data-name=\"' + toCurrencyList[index]["selected_name"] + '\" data-image=\"' + toCurrencyList[index]["image_sm_url"] +  '\">';
                html_top_to += '<div class="row item__wrapper" style="height:42px;line-height:42px;">' +
                    '<div class="col-xs-4 coin_short text-right text-uppercase">' + toCurrencyList[index]["symbol"] + '</div>' +
                    '<div class="col-xs-2 icon__wrapper">' +
                    '<img class="glyphicon coin_icon" height="20" width="20" src="' + toCurrencyList[index]["image_sm_url"] +'" alt="xmr">' +
                    '</div>' +
                    '<div class="col-xs-6 coin_full" uib-tooltip="' +  toCurrencyList[index]["display_name"] + 'tooltip-append-to-body="true" tooltip-placement="right" tooltip-popup-delay="10">' + toCurrencyList[index]["display_name"] + '</div>' +
                    '</div>' +
                    '</li>';
                should_log_to_currency_search = 0;
            } else {
                if(from_symbol != toCurrencyList[index]["symbol"]) {
                    shouldAddNotSupportedSection = true
                    html_not_supported += '<li class="disabled lg-pl-0" data-id=\"' + toCurrencyList[index]["symbol"] +  '\" data-name=\"' + toCurrencyList[index]["selected_name"] + '\">';
                    html_not_supported += '<div class="row item__wrapper" style="height:42px;line-height:42px;">' +
                        '<div class="col-xs-4 coin_short text-right text-uppercase">' + toCurrencyList[index]["symbol"] + '</div>' +
                        '<div class="col-xs-2 icon__wrapper">' +
                        '<img class="glyphicon coin_icon" height="20" width="20" src="' + toCurrencyList[index]["image_sm_url"] +'" alt="xmr">' +
                        '</div>' +
                        '<div class="col-xs-6 coin_full" uib-tooltip="' +  toCurrencyList[index]["display_name"] + 'tooltip-append-to-body="true" tooltip-placement="right" tooltip-popup-delay="10">' + toCurrencyList[index]["display_name"] + '</div>' +
                        '</div>' +
                        '</li>';
                }
            }
        });

        if(should_log_to_currency_search && window.mixpanel && window.is_prod) {
            mixpanel.track("To_Currency", {
                "Currency": from_currency_symbol + ":" + toCurrencyFilter
            });
        }

        if(shouldAddNotSupportedSection) {
            html_top_to += html_not_supported;
        }

        $(".dropdown-menu-to").html(html_top_to);

        $(".dropdown-item-to").click(function(e) {
            e.preventDefault();
            window.is_inactive = false;
            hideAlert();
            var symbol = $(this).attr("data-id");
            var name = $(this).attr("data-name");
            var image = $(this).attr("data-image");
            var from_symbol = $(".selected_currency").attr("data-symbol");
            if(symbol == from_symbol) {
                swap();
            } else {
                $(".conversion_currency").html(name);
                $(".conversion_currency").attr("data-symbol", symbol);
                $(".default_to_img").attr("src", image);
                $("[name='to_currency']").val(symbol);
                get_currency_exchange($(".conversion_currency").attr("data-symbol"), $(".selected_currency").attr("data-symbol"), $(".selected_currency_amount").val());
            }
        })
        $("li.disabled").click(function(e) {
            e.preventDefault();
            e.stopPropagation();
        });
        $("li.non-clickable").click(function(e) {
            e.preventDefault();
            e.stopPropagation();
        });
    }

    var handleCurrencyList = function() {
        var currency_list = window.currency_list;
        handleFromCurrencyList();
        handleToCurrencyList();
        var html_top_to = "";

        $("[name='to_currency']").val(window.default_to_symbol);
        $("[name='from_currency']").val(window.default_from_symbol);


        $(".img-change").click(function(e) {
            e.preventDefault();
            swap();
        });

        $("#fromFilterCurrency").on('change keyup paste', function () {
            window.is_inactive = false;
            // console.log("fromFilterCurrency:change keyup paste triggered");
            handleFromCurrencyList($("#fromFilterCurrency").val());
        });

        $( "#toFilterCurrency").on('change keyup paste', function () {
            window.is_inactive = false;
            // console.log("toFilterCurrency:change triggered");
            handleToCurrencyList($("#toFilterCurrency").val());
        });

        $('.selected_currency_amount').change(function() {
            window.is_inactive = false;
            get_currency_exchange($(".conversion_currency").attr("data-symbol"), $(".selected_currency").attr("data-symbol"), $(".selected_currency_amount").val());
        });


        $("#fromFilterCurrency").click(function(e){
            e.preventDefault();
            e.stopPropagation();
        })

        $("#toFilterCurrency").click(function(e){
            e.preventDefault();
            e.stopPropagation();
        })

        setTimeout(function () {
            get_currency_exchange($(".conversion_currency").attr("data-symbol"), $(".selected_currency").attr("data-symbol"), $(".selected_currency_amount").val());
        }, 600)

    };

    var appendDropdownToBody = function(){
        var dropdownMenu;

        // and when you show it, move it to the body
        $(window).on('show.bs.dropdown', function (e) {

            // grab the menu
            dropdownMenu = $(e.target).find('.dropdown-menu');

            // detach it and append it to the body
            $('body').append(dropdownMenu.detach());

            // grab the new offset position
            var eOffset = $(e.target).offset();

            // make sure to place it where it would normally go (this could be improved)
            dropdownMenu.css({
                'display': 'block',
                'top': eOffset.top + $(e.target).outerHeight(),
                'right': $( window ).width() - eOffset.left - $(e.target).width()
            });
        });

        // and when you hide it, reattach the drop down, and hide it normally
        $(window).on('hide.bs.dropdown', function (e) {
            $(e.target).append(dropdownMenu.detach());
            dropdownMenu.hide();
        });
    };

    var handleFAQScroll = function() {
        $( window ).load(function() {
            var hash = window.location.hash.replace(/^#!/, '#');
            if(hash && !(hash.indexOf("/") > 0) && $(hash + ".accordion-link").offset()) {
                if(hash == '#faq') {
                    $('html, body').animate({
                        scrollTop: $(hash + ".accordion-link").offset().top - 60
                    }, 200);
                } else {
                    $('html, body').animate({
                        scrollTop: $(hash + ".accordion-link").offset().top - 150
                    }, 200);
                }

                if($(hash + ".accordion-link")){
                    $(hash+".accordion-link").trigger("click");
                }
            }
            $("a.accordion-link").click(function(e) {
                window.location.hash = "#!" + $(this).attr("id");
                e.preventDefault();
            });
        });
    };

    var handleAlertMessage = function(){
        if(window.message) {
            showAlert(window.message)
        }
    };

    var handleContinueApp = function() {
        $(".continue-button").click(function() {
            if(window.mixpanel && window.is_prod) {
                mixpanel.track("Home_Continue");
            }
        })
    };

    /* Initialize
     * ------------------------------------------------------ */
    (function ssInit() {
        ssSmoothScroll();
        // ssWaypoints();
        // ssPlaceholder();
        // ssAlertBoxes();
        // ssAOS();
        // ssAjaxChimp();
        // ssBackToTop();
        handleCurrencyList();
        handleAlertMessage();
        // appendDropdownToBody();
        handleFAQScroll();
        // handleContinueApp();
        // myFunction();
        // myFunction2();
    })();


})(jQuery);