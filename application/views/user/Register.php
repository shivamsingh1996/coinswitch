<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <title>Login</title>
   
    <!-- mobile specific metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?=base_url()?>public/css/lib/bootstrap.mine86f.css?v=2018021514">
    <link rel="stylesheet" href="<?=base_url()?>public/css/lib-stylese86f.css?v=2018021514">
    <link rel="stylesheet" href="<?=base_url()?>public/css/style.css">

    <!--<link rel="stylesheet" href="/<?=base_url()?>public/css/app.min.css?v=2018021514">-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>public/css/homepage_v2.mine86f.css?v=2018021514"/>

    <!-- favicons
    ================================================== -->
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=base_url()?>public/images/favicon_new.png" type="image/x-icon">-->

    <!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
        0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("02c8ea110f5d7c6dcf9180c69ebdc9ce");</script><!-- end Mixpanel -->
    <!-- Reddit Conversion Pixel -->
    <script>
        var i=new Image();i.src="../alb.reddit.com/t6130.gif?q=CAAHAAABAAoACQAAAAAQ6xriAA==&amp;s=Z7rm6uk8v_T5QQS3KWVX_GGc-VgXuM36mIh8XxQES58=";
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="../alb.reddit.com/t6130.gif?q=CAAHAAABAAoACQAAAAAQ6xriAA==&amp;s=Z7rm6uk8v_T5QQS3KWVX_GGc-VgXuM36mIh8XxQES58="/>
    </noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Reddit Conversion Pixel -->
</head>
<body class="font" style="-webkit-font-smoothing: antialiased;" >
<script>
    window.is_inactive = true;
    window.is_prod = '1';
    if(is_prod == '1') {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-100639244-1', 'auto');
        ga('send', 'pageview', "/home" + window.location.pathname);
//        ga('send', 'pageview');
        <!-- Facebook Pixel Code -->
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
                document, 'script', '../connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1570069006632609', {
            em: 'care@coinswitch.co'
        });
        fbq('track', 'PageView');

        // mix panel
        if (window.mixpanel) {
            mixpanel.track("Page_View");
        }

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1490579831058785',
                xfbml      : true,
                version    : 'v2.10'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "../connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
</script>

   
    <div style="background-color: black">
        <div class="nav-border position-fixed">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"
                                style="float: left;margin-left: 10px;">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- <a href="index.html"><img class="navbar-brand nav-image" src="<?=base_url()?>public/images/Logo.svg" ></a> -->
                        <h3>LOGO</h3>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right" style="margin:7.5px -15px;">
                            <!--<li class="width  font-OS-Reg"><a href="#">Track Order</a></li>-->
                            <li class="width font-OS-Reg"><a  href="index.html">HOME</a></li>
                            
                            <li class="width  font-OS-Reg"><a class="smoothscroll" href="#">FAQ</a></li>
                            <li class="width  font-OS-Reg"><a href="signin.html">LOGIN</a></li>
                            <li class="width  font-OS-Reg"><a href="signup.html">SIGN UP</a></li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
        <style>
        .box{
            margin: 0 auto;
    padding: 35px 0 30px;
    float: none;
    width: 400px;
    box-shadow: 0 0 6px 2px rgba(0,0,0,.1);
    border-radius: 5px;
    background: rgba(255,255,255,.65);
        }
    </style>
    <main style="background-image: url('https://www.coinswitch.co/public/images/offers_bg.jpg');min-height: 550px;">
    <div class="container" style="margin-top: 67px;">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3" style="padding-top: 40px;">
                        <div class="box" style="padding-bottom:15px;">
            <div class="content-wrap">
                <h2 class="lg-mt-0" style="text-align: center;">Login</h2>
                <?php
                    if(isset($flash)){
                        echo $flash;
                    }
                ?>
                <form action="<?php echo base_url(); ?>index.php/exchange/signup" method="POST" novalidate="" style="margin-bottom: 0rem;" class="ng-pristine ng-valid-email ng-invalid ng-invalid-required">
                    <div class="row">

                        <div class="col-md-11 col-xs-11" style="margin: 20px;">
                         <div class="form-group">
                                <input type="text" class="form-control old-theme-input override-input-css" name="name" placeholder="Your Name" required="" style="padding-right: 9rem;">
                                <?=form_error('name');?>
                               
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control old-theme-input override-input-css" name="email" placeholder="Your Email" required="" style="padding-right: 9rem;">
                                <?=form_error('email');?>
                            </div>
                            <div class="form-group">
                                <input autocomplete="off" class="form-control override-input-css" id="Password" name="password" placeholder="Your Password" type="password" required="">
                                <?=form_error('password');?>
                               
                            </div>
                            <div class="form-group">
                                <input autocomplete="off" class="form-control override-input-css" id="Password" name="confirm_password" placeholder="Confirm Password" type="password" required="">
                               <?=form_error('confirm_password');?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11" style="margin:20px;">
                            <div style="text-align: center;">
                                <input style="font-size:16px;" type="submit" name="register" value="Register" class="old-theme-button gbtn btn-green override-btn col-lg-12" >
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <span class="no-account" style="text-align: center;">
                            <p>
                                <a href="forgetpassword.html">Forgot Password?</a>
                            </p>
                        </span>
                    </div>
                    <div class="row">
                    <span class="no-account" style="text-align: center;">
                            <p>
                                Don't have an account?<a href="signup.html">Register</a>
                            </p>

                        </span>
                </div>
                </form>
                
            </div>
        </div>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
</main>
    <div class="footer font" style="display: none;">
        <div class="container padding">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 hidden-xs" style="text-align:left;">
                    <h3 class="footer-font-14 font-OS-Bold"><a class="font-white" href="index.html">Home</a></h3>
                    <h3 class="footer-font-14 font-OS-Bold"><a class="smoothscroll font-white" href="#">FAQ </a></h3>
                    <h3 class="footer-font-14 font-OS-Bold"><a class="font-white" href="#" target="_blank">Terms </a></h3>
                    
                    
                    <h3 class="footer-font-14 font-OS-Bold"><a class="font-white" href="#">Login / Register</a></h3>
                </div>

                <div class="col-xs-12 visible-xs">
                    <div class="footer-font-14 font-OS-Reg">
                        <p><a class="font-white" href="index.html">Home </a> | <a class="font-white" href="#"> FAQ </a> | <a class="font-white" target="_blank" href="#">
                            Terms</a> | <a class="font-white" href="#">Login / Register</a></p>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <h3 class="footer-heading footer-font-14 font-OS-Bold">LOGO</h3>

                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <h3 class="footer-heading footer-font-14 font-OS-Bold"></h3>

                   
                </div>
            </div>
        </div>
    </div>
    <script data-cfasync="false" src="cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script><script src="<?=base_url()?>public/js/jquery-2.1.3.mine86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/lib/bootstrap.mine86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/pluginse86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/homepagee86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/lib/le.mine86f.js?v=2018021514"></script>
<script>
    LE.init('c06b54a6-de95-46f4-96cb-38c17286758f');
    function trackJavaScriptError(e) {
        var ie = window.event || {},
                errMsg = e.message || ie.errorMessage;
        var errSrc = (e.filename || ie.errorUrl) + ': ' + (e.lineno || ie.errorLine);

        /*** SEND TO SERVER***/
        LE.log('JavaScript Error :: ' + errMsg + ' in: ' + errSrc);
    }
    /**
     * Cross-browser event listener
     */
    if (window.addEventListener) {
        window.addEventListener('error', trackJavaScriptError, false);
    } else if (window.attachEvent) {
        window.attachEvent('onerror', trackJavaScriptError);
    } else {
        window.onerror = trackJavaScriptError;
    }

    !function () {
        var t;
        if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
                t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
                t.factory = function (e) {
                    return function () {
                        var n;
                        return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                    };
                }, t.methods.forEach(function (e) {
            t[e] = t.factory(e);
        }), t.load = function (t) {
            var e, n, o, i;
            e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
        });
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.config({
        enableWelcomeMessage: false,
    })

    var show_welcome_message = function() {
        if (window.is_inactive) {
            drift.api.showWelcomeMessage();
        }
    }

  

        if ($('#specsallA').is(':visible')) {
            // do your special stuff here
        }
    });

    if($(window).width() > 980) {
        <!-- Start of Async Drift Code -->
        drift.load('hzea56fkmf94');
        drift.on('ready', function (api) {
            // Open
            drift.on('awayMessage:open', function (event) {
                window.location.hash = "help";
            })
            drift.on('welcomeMessage:open', function (event) {
                window.location.hash = "help";
            })
//
//            drift.on('startConversation', function (event) {
//                window.location.hash = "help";
//            })

            // Close
            drift.on('awayMessage:close', function (event) {
                window.location.hash = '';
            })
            drift.on('welcomeMessage:close', function (event) {
                window.location.hash = '';
            })
            drift.on('sidebarClose', function (event) {
                window.location.hash = '';
                setTimeout(function() {
                    drift.api.hideWelcomeMessage();
                    drift.api.hideAwayMessage();
                }, 20);
            })

//            setTimeout(function() {
//                show_welcome_message();
//            }, 60000);
        })
    }
    VERSION = '2018021514';
</script>
<!--<script src="/<?=base_url()?>public/js/libJS.min.js?v=2018021514"></script>-->
<!--<script src="/<?=base_url()?>public/js/app.js?v=2018021514"></script>-->
</body>

</html>
</body>
          