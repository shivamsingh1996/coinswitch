<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends CI_Model {
	public function submit(){
		$field1=array(
			'email'=>$this->input->post('email'),
			'password'=>$this->input->post('password'),
			
		);
		$this->db->insert('tbl_users',$field1);
		$a= $this->db->insert_id();

		if($this->db->affected_rows() > 0){
			$data = array('id' =>$a);
			$this->session->set_userdata($data);
			return true;

		}
		else
		{
			return false;
		}
	}
	public function update($id){
		$field=array(
			'fname'=>$this->input->post('First_Name'),
			'lname'=>$this->input->post('Last_Name'),
			'dob'=>$this->input->post('Dob'),
			'country'=>$this->input->post('Country'),
			);
			$this->db->where('id',$id);
			$this->db->update('tbl_users',$field);
		if($this->db->affected_rows() > 0){

			return true;

		}
		else
		{
			return false;
		}
	
	}
}
